class SavePatternParser:
    """
    Class for creating download names based on pattern

    :attr pattern: Pattern used to create names
    """
    pattern: str

    def __init__(self, pattern: str):
        self.pattern = pattern

    def make_pattern(self, post: dict, index: int) -> str:
        """
        Make download name based on pattern

        :param post: Post to download
        :param index: Index of post
        :return: Name for post
        """
        res = self.pattern
        res = res.replace('{md5}', str(post.get('md5') or ''))
        res = res.replace('{id}', str(post.get('id') or ''))
        res = res.replace('{rating}', str(post.get('rating') or ''))
        res = res.replace('{index}', str(index))
        return res
