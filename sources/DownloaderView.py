import tkinter
import re
from tkinter import ttk, N, W, E, S, NSEW, EW
from tkinter import filedialog
from Utils import *
import datetime

from typing import Callable, Optional, List, Tuple


class UserInterruptionException(Exception):
    pass


class DownloaderView:
    """
    GUI of downloader

    :attr root:        Root for tkinter
    :attr mainframe:   Main frame for other widgets
    :attr _handlers:   Dictionary with necessary functions from DownloaderLogic
    :attr _interrupt:  Was download interrupted by user
    :attr _vars:  Even if tkinter variable is bind to label by textvariable it can still be removed by GC
        after going out of scope. So I'm keeping them here.
    :attr _pool_regex: Compiled regex to validate pools input
    :attr _vcmd:       Registered validation function for just digits input
    """
    root: tkinter.Tk
    mainframe: tkinter.Frame
    _handlers: dict
    _interrupt: bool
    _vars: dict
    _pool_regex: any  # TODO what type?
    _vcmd: any  # TODO what type?

    @staticmethod
    def chain_callback_functions(actions: List[Tuple[bool, Callable[[Callable], None]]], callback: Callable) -> None:
        """
        Chain n functions with callback to be executed sequentially

        :param actions: List of functions to be called with variable that decides whether it should be called
        :param callback: Function to call at the end of whole chain
        """

        def make_closure(action: Callable[[Callable], None], fun: Callable) -> Callable:
            """
            Enlarge the chain of functions by adding action at the beginning

            It needs to be in separate function, so fun has a proper closure scope.
            :param action: Action to chain
            :param fun: Current chain
            :return: Longer chair
            """
            return lambda: action(lambda: fun())

        actions_to_do = [action[1] for action in actions if action[0]]
        i = len(actions_to_do) - 1
        f = lambda: callback()
        while i >= 0:
            f = make_closure(actions_to_do[i], f)
            i -= 1
        f()

    def __init__(self, root: tkinter.Tk, handlers: dict):
        self.root = root
        self.root.geometry("500x500")
        # self.root.resizable(0,0) TODO should it be resizable?
        self.root.columnconfigure(0, weight=1)
        self.root.rowconfigure(0, weight=1)
        self._init_frame()
        self._handlers = handlers
        self._interrupt = False
        sets = [item[1] for item in self._handlers['get_sets']()]
        self._vars = {
            'download': tkinter.BooleanVar(),
            'upvote': tkinter.BooleanVar(),
            'fav': tkinter.BooleanVar(),
            'path': tkinter.StringVar(),
            'error': tkinter.StringVar(),
            'ssets': sets,
            'chosen_sets': tkinter.StringVar(value=sets),
        }
        self._pool_regex = re.compile('^[0-9]+(\s*,\s*[0-9]+)*$')
        self._vcmd = (root.register(self.pos_int_tkinter_validate),
                      '%d', '%i', '%P', '%s', '%S', '%v', '%V', '%W')

    def _get_non_download_actions(self, action_filter: Filter) -> List[Tuple[bool, Callable[[Callable], None]]]:
        selected_set = self._get_selected_set()
        set_id = selected_set[0] if selected_set is not None else None
        set_name = selected_set[1] if selected_set is not None else None
        return [
            (
                self._vars['upvote'].get(),
                lambda c: self._draw_progress_screen('Upvoting posts', self._handlers['upvote_posts'](action_filter), c)
            ),
            (
                self._vars['fav'].get(), lambda c: self._draw_progress_screen('Adding posts to favorites',
                                                                              self._handlers['fav_posts'](
                                                                                  action_filter), c)
            ),
            (
                selected_set is not None, lambda c: self._draw_progress_screen(f'Adding posts to set {set_name}',
                                                                               self._handlers[
                                                                                   'add_posts_to_set'](
                                                                                   set_id,
                                                                                   action_filter), c)
            ),
        ]

    # noinspection PyUnusedLocal
    @staticmethod
    def pos_int_tkinter_validate(action, index, value_if_allowed,
                                 prior_value, text, validation_type, trigger_type, widget_name) -> bool:
        """
        Validate only digits input. Args required by tkinter
        """
        if text in '0123456789':
            try:
                int(value_if_allowed)
                return True
            except ValueError:
                return False
        return False

    @staticmethod
    def _validate(to_val: List[Callable[[], Optional[str]]], callback: Callable,
                  validation_error_callback: Callable[[List[str]], None]) -> None:
        """
        Validate form fields

        :param to_val: List of validation functions to call. Function return error to display when validation fails
        :param callback:  Callback if all validations pass
        :param validation_error_callback: Callback if any validation fails
        """
        all_valid = True
        errors = []
        for func in to_val:
            error = func()
            if error is not None:
                errors.append(error)
                all_valid = False
        if not all_valid:
            validation_error_callback(errors)
        else:
            callback()

    def _init_frame(self) -> None:
        """
        Initialize main frame that is base for other widgets
        """
        self.mainframe = ttk.Frame(self.root, padding="10 10 10 10")
        self.mainframe.grid(sticky=(N, S, E, W))

    def _clear_frame(self) -> None:
        """
        Destroy everything on frame before drawing new window
        """
        for child in self.mainframe.winfo_children():
            child.destroy()
        self.mainframe.destroy()
        self._init_frame()

    def _progress(self, gen: Progress,
                  length: int, callback: Callable, error_callback: Callable[[str], None]) -> None:
        """
        Make one step in downloading process

        :param gen: Generator that downloads files
        :param length: Predicted amount of files to download
        :param callback: Callback after successful download
        :param error_callback: Callback after failed or interrupted download
        """
        try:
            if self._interrupt:
                self._interrupt = False
                raise UserInterruptionException()
            (value, _) = next(gen)
            self._vars['progress_var'].set(self._vars['progress_var'].get() + 1)
            self._vars['label_text'].set('Progress: ' + str(value) + '/' + str(length))
            self.root.update()
            self.root.after(10, lambda: self._progress(gen, length, callback, error_callback))
        except StopIteration:
            callback()
        except UserInterruptionException:
            error_callback('User interrupted downloading')
        except Exception as e:
            log(str(e))
            error_callback('Error during download. Details in log file.')

    def _draw_progress_screen(self, title: str, gen: Progress,
                              callback: Callable) -> None:
        """
        Draw screen of progress

        :param title: Title of the screen
        :param gen: Generator that downloads posts
        :param callback: Callback after download successfully ends
        """
        self._clear_frame()
        self.mainframe.columnconfigure(0, weight=1)
        self.mainframe.rowconfigure(0, weight=4)
        self.mainframe.rowconfigure(1, weight=1)
        self.mainframe.rowconfigure(2, weight=1)
        self.mainframe.rowconfigure(3, weight=2)
        self.mainframe.rowconfigure(4, weight=1)

        self._vars['progress_var'] = tkinter.IntVar()
        self._vars['label_text'] = tkinter.StringVar()
        self._vars['error_text'] = tkinter.StringVar()
        (value, length) = next(gen)
        self._vars['label_text'].set('Progress ' + str(value) + '/' + str(length))
        self._vars['error_text'].set('')

        ttk.Label(self.mainframe, text=title, anchor=tkinter.CENTER).grid(column=0, row=0,
                                                                          sticky=EW)
        ttk.Progressbar(self.mainframe, orient='horizontal', maximum=length, value=value,
                        length=200, variable=self._vars['progress_var'], mode='determinate').grid(
            column=0, row=1, sticky=EW)
        ttk.Label(self.mainframe, textvariable=self._vars['label_text'], anchor=tkinter.CENTER).grid(column=0, row=2,
                                                                                                     sticky=EW)
        ttk.Label(self.mainframe, foreground='red', textvariable=self._vars['error_text'], anchor=tkinter.CENTER).grid(
            column=0, row=3, sticky=EW)

        def stop():
            self._interrupt = True

        button = ttk.Button(self.mainframe, text='Stop', command=stop)
        button.grid(column=0, row=4, sticky=EW)

        def error_callback(e: str):
            self._vars['error_text'].set(e)
            self.root.update()
            button['text'] = 'Back'
            button['command'] = self.draw_tag_menu

        self.root.update()
        self._progress(gen, length, callback, error_callback)

    def _draw_download_pools(self, pools: tkinter.StringVar, path: tkinter.StringVar, callback: Callable) -> None:
        """
        Draw screens for downloading of selected pools

        :param pools: Pools to download. Not parsed yet.
        :param path: Path where pools should be downloaded
        :param callback: Callback after successful download
        """
        pools = map(int, pools.get().split(','))
        path = path.get()

        def pool_progress():
            try:
                pool = next(pools)
                pool_name = self._handlers['get_pool_title'](pool)
                action_filter = ('pool', {'pool_id': pool})
                download_posts = lambda: self._handlers['download_posts'](action_filter,
                                                                          os.path.join(path, pool_name))
                self.chain_callback_functions([
                    (
                        self._vars['download'].get(),
                        lambda c: self._draw_progress_screen(f"Downloading pool: {pool_name}", download_posts(), c)
                    ),
                    *self._get_non_download_actions(action_filter)
                ], pool_progress)
            except StopIteration:
                callback()

        pool_progress()

    def _get_selected_set(self) -> Optional[Tuple[int, str]]:
        selection = self._vars['set_box'].curselection()
        ids = self._handlers['get_sets']()
        if len(selection) == 0:
            return None
        else:
            return ids[selection[0]]

    def _draw_main_fields(self, start_row: int, action: Callable) -> None:
        """
        Draw main fields that are the same at each download menu

        :param start_row: From which row start drawing
        :param action: Command for execute button
        """
        self.mainframe.rowconfigure(start_row, weight=0)
        self.mainframe.rowconfigure(start_row + 1, weight=0)
        self.mainframe.rowconfigure(start_row + 2, weight=0)
        self.mainframe.rowconfigure(start_row + 3, weight=0)
        self.mainframe.rowconfigure(start_row + 4, weight=0)
        self.mainframe.rowconfigure(start_row + 5, weight=0)
        self.mainframe.rowconfigure(start_row + 6, weight=0)

        self._vars['download'].set(True)
        self._vars['upvote'].set(False)
        self._vars['fav'].set(False)
        self._vars['path'].set(download_path())
        self._vars['error'].set('')

        ttk.Label(self.mainframe, text='Path:').grid(column=0, row=start_row, sticky=(N, W))
        ttk.Entry(self.mainframe, textvariable=self._vars['path']).grid(column=1, row=start_row, sticky=(N, EW))
        ttk.Button(self.mainframe, text="Select path",
                   command=lambda: self._vars['path'].set(
                       filedialog.askdirectory(initialdir=self._vars['path'].get()))).grid(column=2, row=start_row,
                                                                                           sticky=(N, W))

        ttk.Label(self.mainframe, text='Download posts:').grid(column=0, row=start_row + 1, sticky=(N, W))
        ttk.Checkbutton(self.mainframe, variable=self._vars['download']).grid(column=1, row=start_row + 1,
                                                                              sticky=(N, W))

        ttk.Label(self.mainframe, text='Upvote posts:').grid(column=0, row=start_row + 2, sticky=(N, W))
        ttk.Checkbutton(self.mainframe, variable=self._vars['upvote'],
                        state=tkinter.NORMAL if self._handlers['get_config']()['useLogin'] else tkinter.DISABLED).grid(
            column=1, row=start_row + 2, sticky=(N, W))

        ttk.Label(self.mainframe, text='Add posts to favorites:').grid(column=0, row=start_row + 3, sticky=(N, W))
        ttk.Checkbutton(self.mainframe, variable=self._vars['fav'],
                        state=tkinter.NORMAL if self._handlers['get_config']()['useLogin'] else tkinter.DISABLED).grid(
            column=1, row=start_row + 3, sticky=(N, W))

        ttk.Label(self.mainframe, text='Add posts to set:').grid(column=0, row=start_row + 4, sticky=(N, W))
        self._vars['set_box'] = tkinter.Listbox(self.mainframe, listvariable=self._vars['chosen_sets'], height=5,
                                                selectmode='browse')
        self._vars['set_box'].grid(column=1, row=start_row + 4, sticky=(N, W))

        ttk.Label(self.mainframe, textvariable=self._vars['error'], foreground='red', anchor=tkinter.CENTER).grid(
            column=0,
            columnspan=2, row=start_row + 5, sticky=EW)

        ttk.Button(self.mainframe, text="Execute",
                   command=action).grid(column=0, row=start_row + 6, sticky=(N, W))
        ttk.Button(self.mainframe, text="Back",
                   command=lambda: self.draw_main_menu()).grid(column=2, row=start_row + 6, sticky=(N, E))

    def _is_any_action_selected(self) -> bool:
        """
        Check if any action was selected on menu

        :return: True if any action was selected, false otherwise
        """
        return self._vars['download'].get() or self._vars['fav'].get() or self._vars[
            'upvote'].get() or self._get_selected_set() is not None

    def draw_pool_menu(self) -> None:
        """
        Draw menu for downloading pools
        """
        self._clear_frame()
        self.mainframe.columnconfigure(0, weight=5)
        self.mainframe.columnconfigure(1, weight=5)
        self.mainframe.columnconfigure(2, weight=1)
        self.mainframe.rowconfigure(0, weight=0)

        pools = tkinter.StringVar()

        ttk.Label(self.mainframe, text='Pools numbers separated by comma:').grid(column=0, row=0, sticky=(N, W))
        ttk.Entry(self.mainframe, textvariable=pools).grid(column=1, row=0, sticky=(N, EW))

        self._draw_main_fields(1, lambda: self._validate(
            [
                lambda: 'No action selected' if not self._is_any_action_selected() else None,
                lambda: 'Bad format of pools list' if not self._pool_regex.match(pools.get()) else None,
                lambda: 'Path is invalid' if not is_dir_path_valid(self._vars['path'].get()) else None
            ],
            lambda: self._draw_download_pools(pools, self._vars['path'], self.draw_main_menu),
            lambda tab: self._vars['error'].set(', '.join(tab))
        ))

    def _draw_download_tags(self, tags: tkinter.StringVar, path: tkinter.StringVar) -> None:
        """
        Draw screens for downloading of tags

        :param tags: Tags to download
        :param path: Path where posts should be downloaded
        """
        tags = tags.get()
        path = path.get()
        action_filter = ('tags', {'tags': tags})
        download_posts = lambda: self._handlers['download_posts'](action_filter, path)
        self.chain_callback_functions([
            (
                self._vars['download'].get(),
                lambda c: self._draw_progress_screen(f"Downloading tags {tags}", download_posts(), c)
            ),
            *self._get_non_download_actions(action_filter)
        ], self.draw_main_menu)

    def _draw_download_popular(self, path: tkinter.StringVar, pop_type: tkinter.StringVar, year: tkinter.IntVar,
                               month: tkinter.IntVar, day: tkinter.IntVar) -> None:
        """
        Draw screens for downloading of popular tags

        :param pop_type: Type of popular
        :param year: Year
        :param month: Month
        :param day: Day
        :param path: Path where posts should be downloaded
        """
        vpath = path.get()
        vtype = pop_type.get().lower()
        vyear = year.get()
        vmonth = month.get()
        vday = None if vtype == 'month' else day.get()

        action_filter = ('popular', {'type': vtype, 'year': vyear, 'month': vmonth, 'day': vday})
        download_posts = lambda: self._handlers['download_posts'](action_filter, vpath)
        self.chain_callback_functions([
            (
                self._vars['download'].get(),
                lambda c: self._draw_progress_screen(f"Downloading popular by {vtype}", download_posts(), c)
            ),
            *self._get_non_download_actions(action_filter)
        ], self.draw_main_menu)

    def draw_tag_menu(self) -> None:
        """
            Draw menu for downloading tags
        """
        self._clear_frame()
        self.mainframe.columnconfigure(0, weight=5)
        self.mainframe.columnconfigure(1, weight=5)
        self.mainframe.columnconfigure(2, weight=1)
        self.mainframe.rowconfigure(0, weight=0)

        tags = tkinter.StringVar()

        ttk.Label(self.mainframe, text='Tags:', anchor=E).grid(column=0, row=0, sticky=(N, W))
        ttk.Entry(self.mainframe, textvariable=tags).grid(column=1, row=0, sticky=(N, EW))

        self._draw_main_fields(1, lambda: self._validate(
            [
                lambda: 'No action selected' if not self._is_any_action_selected() else None,
                lambda: 'No tags selected' if len(tags.get()) == 0 else None,
                lambda: 'Path is invalid' if not is_dir_path_valid(self._vars['path'].get()) else None
            ],
            lambda: self._draw_download_tags(tags, self._vars['path']),
            lambda tab: self._vars['error'].set(', '.join(tab))
        ))

    def draw_popular_menu(self) -> None:
        """
            Draw menu for downloading popular posts
        """
        self._clear_frame()
        self.mainframe.columnconfigure(0, weight=5)
        self.mainframe.columnconfigure(1, weight=5)
        self.mainframe.columnconfigure(2, weight=1)
        self.mainframe.rowconfigure(0, weight=0)
        self.mainframe.rowconfigure(1, weight=0)
        self.mainframe.rowconfigure(2, weight=0)
        self.mainframe.rowconfigure(3, weight=0)
        self.mainframe.rowconfigure(4, weight=0)
        self.mainframe.rowconfigure(5, weight=0)
        self.mainframe.rowconfigure(6, weight=0)

        self._vars['type'] = tkinter.StringVar()
        self._vars['year'] = tkinter.IntVar()
        self._vars['month'] = tkinter.IntVar()
        self._vars['day'] = tkinter.IntVar()
        now = datetime.datetime.now()
        self._vars['year'].set(now.year)
        self._vars['month'].set(now.month)
        self._vars['day'].set(now.day)

        ttk.Label(self.mainframe, text='Day:', anchor=E).grid(column=0, row=6, sticky=(N, W))
        day_spinbox = tkinter.Spinbox(self.mainframe, validate='key', validatecommand=self._vcmd, from_=1, to=31,
                                      textvariable=self._vars['day'])
        day_spinbox.grid(column=1, row=6, sticky=EW)

        def enable_day():
            day_spinbox['state'] = tkinter.NORMAL

        def disable_day():
            day_spinbox['state'] = tkinter.DISABLED

        ttk.Label(self.mainframe, text='Type:', anchor=E).grid(column=0, row=0, sticky=(N, W))
        def_radio = ttk.Radiobutton(self.mainframe, text='Month', variable=self._vars['type'], value='month',
                                    command=disable_day)
        def_radio.grid(column=0, row=1, columnspan=3, sticky=(N, EW))
        def_radio.invoke()
        ttk.Radiobutton(self.mainframe, text='Week', variable=self._vars['type'], value='week',
                        command=enable_day).grid(column=0, row=2, columnspan=3, sticky=(N, EW))
        ttk.Radiobutton(self.mainframe, text='Day', variable=self._vars['type'], value='day', command=enable_day).grid(
            column=0, row=3, columnspan=3, sticky=(N, EW))

        ttk.Label(self.mainframe, text='Year:', anchor=E).grid(column=0, row=4, sticky=(N, W))
        tkinter.Spinbox(self.mainframe, validate='key', validatecommand=self._vcmd, from_=2006, to=2100,
                        textvariable=self._vars['year']).grid(column=1, row=4, sticky=EW)

        ttk.Label(self.mainframe, text='Month:', anchor=E).grid(column=0, row=5, sticky=(N, W))
        tkinter.Spinbox(self.mainframe, validate='key', validatecommand=self._vcmd, from_=1, to=12,
                        textvariable=self._vars['month']).grid(column=1, row=5, sticky=EW)

        def get_date():
            yyyy = str(self._vars['year'].get())
            mm = str(self._vars['month'].get()) if self._vars['month'].get() >= 10 else '0' + str(
                self._vars['month'].get())
            dd = str(self._vars['day'].get()) if self._vars['day'].get() >= 10 else '0' + str(
                self._vars['day'].get())
            return f'{yyyy}-{mm}-{dd}'

        self._draw_main_fields(7, lambda: self._validate(
            [
                lambda: 'No action selected' if not self._is_any_action_selected() else None,
                lambda: 'Incorrect date' if not is_date_valid(get_date()) else None,
                lambda: 'Path is invalid' if not is_dir_path_valid(self._vars['path'].get()) else None,
                lambda: 'Date should not be in future' if not is_past_or_current_date(get_date()) else None,
            ],
            lambda: self._draw_download_popular(self._vars['path'], self._vars['type'], self._vars['year'],
                                                self._vars['month'], self._vars['day']),
            lambda tab: self._vars['error'].set(', '.join(tab))
        ))

    def draw_main_menu(self) -> None:
        """
        Draw the main menu
        """
        self._clear_frame()
        self.mainframe.columnconfigure(0, weight=1)
        self.mainframe.rowconfigure(0, weight=2)
        self.mainframe.rowconfigure(1, weight=1)
        self.mainframe.rowconfigure(2, weight=1)
        self.mainframe.rowconfigure(3, weight=1)
        self.mainframe.rowconfigure(4, weight=1)
        self.mainframe.rowconfigure(5, weight=1)
        py = 5
        px = 5
        ttk.Label(self.mainframe, text='Shitty e621 downloader by Granberia', anchor=tkinter.CENTER).grid(column=0,
                                                                                                          row=0,
                                                                                                          sticky=NSEW)
        ttk.Button(self.mainframe, text="Pools",
                   command=lambda: self.draw_pool_menu()).grid(column=0, row=1, sticky=NSEW, pady=py, padx=px)
        ttk.Button(self.mainframe, text="Tags",
                   command=lambda: self.draw_tag_menu()).grid(column=0, row=2, sticky=NSEW, pady=py, padx=px)
        ttk.Button(self.mainframe, text="Popular posts",
                   command=lambda: self.draw_popular_menu()).grid(column=0, row=3, sticky=NSEW, pady=py, padx=px)
        ttk.Button(self.mainframe, text="Configuration",
                   command=lambda: self.draw_options()).grid(column=0, row=4, sticky=NSEW, pady=py, padx=px)
        ttk.Button(self.mainframe, text="Exit",
                   command=lambda: self.root.destroy()).grid(column=0, row=5, sticky=NSEW, pady=py, padx=px)

    def update_config(self, new_vals: dict) -> None:
        """
        Update config with new values

        :param new_vals: New values
        """
        self._handlers['update_config']({k: v.get() for k, v in new_vals.items()})
        self.draw_main_menu()

    def draw_options(self) -> None:
        """
        Draw screen to change config by GUI
        """
        self._clear_frame()
        config_struct = get_config_struct()
        config_data = self._handlers['get_config']()
        options_vars = {}
        self._vars['options'] = options_vars
        vals = []
        error = tkinter.StringVar()
        error.set('')
        self.mainframe.columnconfigure(0, weight=1)
        self.mainframe.columnconfigure(1, weight=1)

        def draw_entry(row: int, _row_data, _row_struct: dict, row_name: str):
            """
            Draw editable widget for a single row of configuration fields

            :param row: Number of row
            :param _row_data: Data for row
            :param _row_struct: Structure for row
            :param row_name: Name for field defined in this row
            """
            if _row_struct['type'] == 'str':
                options_vars[row_name] = tkinter.StringVar()
                options_vars[row_name].set(_row_data)
                ttk.Entry(self.mainframe, textvariable=options_vars[row_name]).grid(column=1, row=row, sticky=EW)
            elif _row_struct['type'] == 'bool':
                options_vars[row_name] = tkinter.BooleanVar()
                options_vars[row_name].set(_row_data)
                ttk.Checkbutton(self.mainframe, variable=options_vars[row_name]).grid(column=1, row=row, sticky=EW)
            elif _row_struct['type'] == 'int':
                options_vars[row_name] = tkinter.IntVar()
                options_vars[row_name].set(_row_data)
                tkinter.Spinbox(self.mainframe, validate='key', validatecommand=self._vcmd, from_=1, to=320,
                                textvariable=options_vars[row_name]).grid(column=1, row=row, sticky=EW)

            if 'validation' in _row_struct:
                vals.append(lambda: _row_struct['validation'](options_vars[row_name].get()))

        def draw_row(row: int, _row_data, _row_struct: dict, row_name: str):
            """
            Draw a single row of configuration fields

            :param row: Number or row
            :param _row_data: Data for row
            :param _row_struct: Structure for row
            :param row_name: Name for field defined in this row
            """
            self.mainframe.rowconfigure(row, weight=1)
            ttk.Label(self.mainframe, text=row_name, anchor=W).grid(column=0, row=row, sticky=EW)
            draw_entry(row, _row_data, _row_struct, row_name)

        i = 0
        while i < len(config_struct.keys()):
            name = list(config_struct.keys())[i]
            row_struct = config_struct[name]
            row_data = config_data[name]
            draw_row(i, row_data, row_struct, name)
            i += 1
        self.mainframe.rowconfigure(i, weight=1)
        self.mainframe.rowconfigure(i + 1, weight=1)
        ttk.Label(self.mainframe, textvariable=error, foreground='red', anchor=tkinter.CENTER).grid(column=0,
                                                                                                    columnspan=2, row=i,
                                                                                                    sticky=EW)
        ttk.Button(self.mainframe, text="Save", command=lambda: self._validate(vals,
                                                                               lambda: self.update_config(options_vars),
                                                                               lambda tab: error.set(', '.join(tab))
                                                                               )).grid(row=i + 1, column=0, sticky=EW)
        ttk.Button(self.mainframe, text="Back", command=self.draw_main_menu).grid(row=i + 1, column=1, sticky=EW)
