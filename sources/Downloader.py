#!/usr/bin/python3

from DownloaderLogic import DownloaderLogic
from DownloaderView import DownloaderView
from Utils import logs_path
import os

import sys
import tkinter
import datetime

downloader = DownloaderLogic()

now = str(datetime.datetime.now())[:10].replace('-', '')
if downloader.config['useLogs']:
    sys.stderr = open(os.path.join(logs_path(), f'log{now}.txt'), 'a+')

root = tkinter.Tk()
root.title("e621 downloader")
handlers = {
    'download_pool': downloader.download_pool,
    'download_tags': downloader.download_tags,
    'get_config': lambda: downloader.config,
    'update_config': downloader.update_config,
    'get_pool_title': downloader.get_pool_title,
    'download_posts': downloader.download_posts,
    'upvote_posts': downloader.upvote_posts,
    'fav_posts': downloader.fav_posts,
    'get_sets': downloader.get_available_sets,
    'add_posts_to_set': downloader.add_posts_to_set,
}
view = DownloaderView(root, handlers)
view.draw_main_menu()
root.mainloop()
