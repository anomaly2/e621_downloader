import sys
import os
from typing import Generator, Tuple
import datetime

Progress = Generator[Tuple[int, int], None, None]
Filter = Tuple[str, dict]


def log(message: str) -> None:
    """
    Log the message
    :param message: Message to be logged
    """
    print(message, file=sys.stderr)


def logs_path() -> str:
    """
    Get absolute path to logs directory
    :return: Absolute path to logs directory
    """
    return os.path.abspath(os.path.join(os.path.dirname(sys.argv[0]), '..', 'logs'))


def download_path() -> str:
    """
    Get absolute path to downloads directory
    :return: Absolute path to downloads directory
    """
    return os.path.abspath(os.path.join(os.path.dirname(sys.argv[0]), '..', 'downloads'))


def get_conf_paths() -> dict:
    """
    Get paths to both local and default configuration files.

    :return: Dictionary with both paths
    """
    base_path = os.path.dirname(sys.argv[0])
    return {
        'default': os.path.join(os.path.join(base_path, os.path.join('..', 'config')), 'conf.json'),
        'local': os.path.join(os.path.join(base_path, os.path.join('..', 'config')), 'local_conf.json'),
        'test': os.path.join(os.path.join(base_path, os.path.join('..', 'config')), 'test_conf.json'),
    }


def is_dir_path_valid(dir_path: str) -> bool:
    """
    Check if give path is valid, already existing directory path

    :param dir_path: Path to check
    :return: True if path is valid, false otherwise
    """
    return os.path.isdir(dir_path) and os.path.exists(dir_path)


def get_config_struct() -> dict:
    """
    Get dictionary with expected structure of configuration file
    :return: Dictionary with type and optional validation for each key in configuration file
    """
    return {
        'useLogin': {'type': 'bool'},
        'login': {'type': 'str'},
        'apiKey': {'type': 'str'},
        'maxTries': {'type': 'int',
                     'validation': lambda i: 'Max' 'MaxTries value has to be a positive integer' if (not is_int(i)) or (
                                 i <= 0) else None},
        'pageLimit': {'type': 'int',
                      'validation': lambda i: 'Page limit is positive integer and less or equal to 320' if (not is_int(
                          i)) or (i <= 0) or (i > 320) else None},
        'verbose': {'type': 'bool'},
        'tagSavePattern': {'type': 'str'},
        'poolSavePattern': {'type': 'str'},
        'useLogs' : {'type' : 'bool'},
    }


def is_int(val: any) -> bool:
    """
    Can value be interpreted as integer

    :param val: Value to check
    :return: True if value can be int, false otherwise
    """
    try:
        int(val)
        return True
    except ValueError:
        return False


def escape_directory_name(name: str) -> str:
    """
    String special characters from directory name

    Escape name of pool, so it can be used as directory on windows.
    It still may fail, but it's unlikely.
    :param name: Directory name
    :return: Directory name stripped from bad characters
    """
    for sign in ('<', '>', ':', '"', '/', '\\', '|', '?', '*'):
        name = name.replace(sign, '')
    return name

def is_date_valid(date_text: str) -> bool:
    """
    Check if string is correct date in format YYYY-MM-DD

    :param date_text: String to check
    :return: True if it's correct date, false otherwise
    """
    try:
        datetime.datetime.strptime(date_text, '%Y-%m-%d')
        return True
    except ValueError:
        return False

def is_past_or_current_date(date_text: str) -> bool:
    """
    Check if string represents date in the past or current date

    :param date_text: String to check
    :return: True if string represents date <= current date, false otherwise
    """
    now = datetime.datetime.now()
    return is_date_valid(date_text) and datetime.datetime.strptime(date_text, '%Y-%m-%d') <= now
