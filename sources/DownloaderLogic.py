from SavePatternParser import SavePatternParser
from Utils import *

import urllib.request
import urllib.parse
import os
import time
import json

from typing import Optional, List, Callable


class DownloaderLogic:
    """
    Logic of downloader

    :attr _poolParser:   Parser for pool downloads pattern
    :attr _tagParser:    Parser for tag downloads pattern
    :attr _config:       Configuration
    :attr _headers:      Headers for request
    :attr _test_mode:    Initialize for tests (use test config path)
    :attr _login_params: Params to add to request if using login
    """
    _poolParser: SavePatternParser
    _tagParser: SavePatternParser
    _config: dict
    _headers: dict
    _test_mode: bool
    _login_params: dict
    _available_sets: List[Tuple[int, str]]

    def __init__(self, test_mode: bool = False):
        self._test_mode = test_mode
        self._headers = {
            'User-Agent': 'ShittyDownloader/0.5 (by Granberia on e621)'
        }
        self._load_config()

    @property
    def config(self) -> dict:
        return self._config

    def _print_if_verbose(self, string: str) -> None:
        """
        Log message if verbosity is selected in config

        :param string:  Message to log
        """
        if self._config['verbose']:
            log(string)

    def _load_config(self) -> None:
        """
        Load the configuration from the file

        If it's test mode, load test file
        Otherwise try loading local file
        If fails load default file
        """
        self._config = {}
        config_path = None
        conf_paths = get_conf_paths()
        if self._test_mode:
            config_path = conf_paths['test']
        elif os.path.isfile(conf_paths['local']):
            config_path = conf_paths['local']
        elif os.path.isfile(conf_paths['default']):
            config_path = conf_paths['default']
        if not config_path:
            log(
                "Error: Configuration file not detected. "
                "You need either original conf.json file or local_conf.json file.")
            exit(-1)
        with open(config_path) as data_file:
            self._config = json.load(data_file)
        self._poolParser = SavePatternParser(self._config['poolSavePattern'])
        self._tagParser = SavePatternParser(self._config['tagSavePattern'])
        self._login_params = {
            'login': self._config['login'],
            'password_hash': self._config['apiKey']
        } if self._config['useLogin'] else {}
        self._available_sets = self.get_sets(self.get_user_id())

    def _get_response(self, request: urllib.request, max_tries: int) -> Optional[any]:
        """
        Get response for request using no more than max_tries

        :param request: Request to make
        :param max_tries: How many tries should be made
        :return: Response to request or None if request failed
        """
        ext_response = self._get_response_ext(request, max_tries, lambda _: False)
        return ext_response[0]

    def _get_response_ext(self, request: urllib.request, max_tries: int,
                          special_error_handler: Callable[[Exception], bool]) -> Tuple[
        Optional[any], Optional[Exception]]:
        """
        Get response for request using no more than max_tries. Handle special cases separately

        :param request: Request to make
        :param max_tries: How many tries should be made
        :param special_error_handler: Should be treated as special case
        :return: Tuple with Response to request or None if request failed and Exception if it's special case or None.
        """
        for tries in range(0, max_tries):
            try:
                response = urllib.request.urlopen(request)
                return response, None
            except Exception as e:
                if special_error_handler(e):
                    return None, e
                self._print_if_verbose("error" + str(e))
                time.sleep(1)
        return None, None

    def _get_popular_post_urls(self, pop_type: str, year: int, month: int, day: Optional[int]) -> Optional[list]:
        """
        Get data for all popular posts

        :param pop_type: Either 'day', 'month' or 'week'
        :param year: Year of date
        :param month: Month of data
        :param day: Day of date. Not used in month type.
        :return: Dictionary with pool's title and posts or None if it failed
        """
        self._print_if_verbose("Getting posts urls...")
        posts = []
        day_param = {'day': day} if day is not None else {}
        params = urllib.parse.urlencode({
            'year': year,
            'month': month,
            **day_param,
            **self._login_params
        })
        print(f'https://e621.net/post/popular_by_{pop_type}.json?' + params)
        pool_list_req = urllib.request.Request(
            f'https://e621.net/post/popular_by_{pop_type}.json?' + params,
            data=None,
            headers=self._headers,
        )
        response = self._get_response(pool_list_req, self._config['maxTries'])
        if not response:
            return None
        html = response.read()
        data = json.loads(html)
        if not data:
            response.close()
        posts += data
        response.close()
        return posts

    def _get_pool_post_urls(self, pool_id: int) -> Optional[list]:
        """
        Get data for all posts in pools, and title of the pool.

        :param pool_id: Id of the pool
        :return: Dictionary with pool's title and posts or None if it failed
        """
        self._print_if_verbose("Getting posts urls...")
        posts = []
        current_page = 1
        while True:
            params = urllib.parse.urlencode({
                'id': str(pool_id),
                'page': str(current_page),
                'limit': str(self._config['pageLimit']),
                **self._login_params,
            })
            pool_list_req = urllib.request.Request(
                'https://e621.net/pool/show.json?' + params,
                data=None,
                headers=self._headers,
            )
            response = self._get_response(pool_list_req, self._config['maxTries'])
            if not response:
                return None
            self._print_if_verbose("Got index of page " + str(current_page))
            html = response.read()
            data = json.loads(html)
            if not data['posts']:
                response.close()
                break
            posts += data['posts']
            current_page += 1
            response.close()
        return posts

    def _get_tag_post_urls(self, posts_tags: str) -> Optional[list]:
        """
        Get data for all posts tagged with give tags.

        :param posts_tags: Tags used for search
        :return: Dictionary with posts or None if failed.
        """
        self._print_if_verbose("Getting posts urls...")
        posts = []
        current_page = 1
        # Not handling over 750 page right now
        while current_page <= 750:
            params = urllib.parse.urlencode({
                'tags': posts_tags,
                'page': str(current_page),
                'limit': str(self._config['pageLimit']),
                **self._login_params,
            })
            pool_list_req = urllib.request.Request(
                'https://e621.net/post/index.json?' + params,
                data=None,
                headers=self._headers,
            )
            response = self._get_response(pool_list_req, self._config['maxTries'])
            if not response:
                return None
            self._print_if_verbose("Got index of page " + str(current_page))
            html = response.read()
            data = json.loads(html)
            if not data:
                response.close()
                break
            posts += data
            current_page += 1
            response.close()
        return posts

    def download_pool(self, pool_id: int, base_path: str) -> Progress:
        """
        Download pool with given id and path

        Download pool with id = poolId to the directory base_path/<escaped comic name>
        Directory will be created if needed
        It does not clean after itself if fails.
        :param pool_id: Id of pool to download.
        :param base_path: To what path the pool should be downloaded
        :yield: Tuple with number of just downloaded item (0 right at start, before downloading starts) and total number
            of items to download.
        """
        title = self.get_pool_title(pool_id)
        destination_path = os.path.join(base_path, title)
        return self.download_posts(('pool', {'pool_id': pool_id}), destination_path)

    def _download_post(self, post_url: str, name_to_save: str, file_ext: str, base_path: str) -> bool:
        """
        Download single post

        :param post_url: url of the post
        :param name_to_save: name of saved file
        :param file_ext: extension of saved file
        :param base_path: path in which directory for pool will be placed
        :return: false when failed true otherwise.
        """
        destination_path = base_path
        if not os.path.exists(destination_path):
            os.makedirs(destination_path)
        file_name = os.path.join(destination_path, name_to_save + '.' + file_ext)
        if os.path.isfile(file_name):
            self._print_if_verbose('File ' + file_name + " already exists. Skipping. \n")
            return True
        for tries in range(0, self._config['maxTries']):
            try:
                urllib.request.urlretrieve(post_url, file_name)
                return True
            except urllib.request.ContentTooShortError:
                time.sleep(1)
        return False

    def download_posts(self, download_filter: Filter, path: str) -> Progress:
        """
        Download posts by given filter

        :param download_filter: Filter used for downloading.
        :param path: Path where posts should be downloaded
        """
        pattern_parser = self._poolParser if download_filter[0] == 'pool' else self._tagParser
        posts = self.get_action_posts(download_filter)
        if posts is None:
            raise Exception('Error in getting posts list')
        curr = 1
        posts_len = len(posts)
        yield (0, posts_len)
        for post in posts:
            self._print_if_verbose('Downloading post ' + str(curr) + ' of ' + str(posts_len))
            if not self._download_post(post['file_url'], pattern_parser.make_pattern(post, curr),
                                       post['file_ext'], path):
                raise Exception('Error in downloading posts for tags')
            yield (curr, posts_len)
            curr += 1

    def download_tags(self, tags_to_download: str, base_path: str) -> Progress:
        """
        Download posts by tags to given path

        Download posts with given tags to the directory base_path
        Unlike download by pool if user wants separate folder they need to create it themselves.
        It does not clean after itself if fails.
        :param tags_to_download: Tags to filter the download
        :param base_path: To what path the pool should be downloaded
        :yield: Tuple with number of just downloaded item (0 right at start, before downloading starts) and total number
            of items to download.
        """
        assert len(tags_to_download) > 0
        download_filter = ('tags', {'tags': tags_to_download})
        return self.download_posts(download_filter, base_path)

    def update_config(self, json_obj: dict) -> None:
        """
        Update config with new values

        :param json_obj: New values
        """
        local_path = get_conf_paths()['test'] if self._test_mode else get_conf_paths()['local']
        json_txt = json.dumps(json_obj, indent=4)
        with open(local_path, 'w+') as json_file:
            json_file.write(json_txt)
        self._load_config()

    def get_pool_title(self, pool_id: int) -> Optional[str]:
        """
        Get title of the pool

        :param pool_id: Id of the pool
        :return: Title of the pool or None if it failed
        """
        self._print_if_verbose("Getting posts urls...")
        params = urllib.parse.urlencode({
            'id': str(pool_id),
            'page': str(1),
            'limit': str(self._config['pageLimit']),
            **self._login_params,
        })
        pool_list_req = urllib.request.Request(
            'https://e621.net/pool/show.json?' + params,
            data=None,
            headers=self._headers,
        )
        response = self._get_response(pool_list_req, self._config['maxTries'])
        if not response:
            return None
        html = response.read()
        data = json.loads(html)
        title = escape_directory_name(data['name'])
        self._print_if_verbose('Got pool title')
        response.close()
        return title

    def upvote_posts(self, download_filter: Filter) -> Progress:
        """
        Upvote list of posts

        :param download_filter: Filter to use
        :return: Progress generator
        """
        assert self._config['useLogin']
        posts_ids = [item['id'] for item in self.get_action_posts(download_filter)]
        params = {
            'score': 1,
            **self._login_params,
        }
        yield (0, len(posts_ids))
        curr = 1
        for post_id in posts_ids:
            params['id'] = post_id
            upvote_request = urllib.request.Request(
                'https://e621.net/post/vote.json?' + urllib.parse.urlencode(params),
                data=None,
                headers=self._headers,
            )
            upvote_request.get_method = lambda: 'POST'
            response = self._get_response(upvote_request, self._config['maxTries'])
            if not response:
                raise Exception('Failed to upvote')
            json_resp = json.loads(response.read())
            if not json_resp['success']:
                raise Exception('Failed to upvote')
            if json_resp['change'] == -1:
                # Post was upvoted before and now it's not. Let's vote again
                self._get_response(upvote_request, self.config['maxTries'])
            response.close()
            yield (curr, len(posts_ids))
            curr += 1

    def fav_posts(self, download_filter: Filter) -> Progress:
        """
        Add list of posts to favorites

        :param download_filter: Filter to use
        :return: Progress generator
        """
        assert self._config['useLogin']
        posts_ids = [item['id'] for item in self.get_action_posts(download_filter)]
        params = {
            'score': 1,
            **self._login_params,
        }
        yield (0, len(posts_ids))
        curr = 1
        for post_id in posts_ids:
            params['id'] = post_id
            fav_request = urllib.request.Request(
                'https://e621.net/favorite/create.json?' + urllib.parse.urlencode(params),
                data=None,
                headers=self._headers,
            )
            fav_request.get_method = lambda: 'POST'
            response_t = self._get_response_ext(fav_request, self._config['maxTries'], lambda e: '423' in str(e))
            response = None
            if response_t[0] is not None:
                response = response_t[0]
            elif response_t[1] is None:
                raise Exception('Error in creating favorite')
            if response:
                self._print_if_verbose(f'Favorited posts with id = {post_id}')
                response.close()
            else:
                self._print_if_verbose(f'Post with id = {post_id} was already in favorites')
            yield (curr, len(posts_ids))
            curr += 1

    def get_user_id(self) -> Optional[int]:
        """
        Get id of user

        :return: id of user or None if not using login
        """
        if not self._config['useLogin']:
            return None
        params = {
            'name': self._config['login'],
            **self._login_params,
        }
        request = urllib.request.Request(
            'https://e621.net/user/show.json?' + urllib.parse.urlencode(params),
            data=None,
            headers=self._headers,
        )
        response = self._get_response(request, self._config['maxTries'])
        if response is None:
            raise Exception("Can't get login id")
        user_id = json.loads(response.read())['id']
        response.close()
        return user_id

    def get_sets(self, user_id: Optional[int]) -> List[Tuple[int, str]]:
        """
        Get set of sets where user can add posts

        :param user_id: id of user for who search set
        :return: set of tuples that contains id and name of sets
        """
        if user_id is None:
            return []
        params = {
            'user_id': user_id,
            **self._login_params,
        }
        request = urllib.request.Request(
            'https://e621.net/set/index.json?' + urllib.parse.urlencode(params),
            data=None,
            headers=self._headers,
        )
        response = self._get_response(request, self._config['maxTries'])
        if response is None:
            raise Exception("Can't get user own sets")
        user_sets = [(item['id'], item['name']) for item in json.loads(response.read())]
        response.close()
        params = {
            'maintainer_id': user_id,
            **self._login_params,
        }
        request = urllib.request.Request(
            'https://e621.net/set/index.json?' + urllib.parse.urlencode(params),
            data=None,
            headers=self._headers,
        )
        response = self._get_response(request, self._config['maxTries'])
        if response is None:
            raise Exception("Can't get user maintainer sets")
        maintainer_sets = [(item['id'], item['name']) for item in json.loads(response.read())]
        response.close()
        sets = list({*user_sets, *maintainer_sets})
        self._print_if_verbose(f'Loaded sets for user with id {user_id}')
        return sets

    def get_available_sets(self) -> List[Tuple[int, str]]:
        return self._available_sets

    def add_posts_to_set(self, set_id: int, download_filter: Filter) -> Progress:
        """
        Add list of posts to set

        :param set_id: Set id
        :param download_filter: Filter to use
        :return: Progress generator
        """
        assert self._config['useLogin']
        posts_ids = [item['id'] for item in self.get_action_posts(download_filter)]
        params = {
            **self._login_params,
        }
        data_params = {
            'set_id': set_id,
        }
        yield (0, len(posts_ids))
        curr = 1
        for post_id in posts_ids:
            data_params['post_id'] = post_id
            set_request = urllib.request.Request(
                'https://e621.net/set/add_post.json?' + urllib.parse.urlencode(params),
                data=urllib.parse.urlencode(data_params).encode("utf-8"),
                headers=self._headers,
            )
            set_request.get_method = lambda: 'POST'
            response_t = self._get_response_ext(set_request, self._config['maxTries'], lambda e: '422' in str(e))
            response = None
            if response_t[0] is not None:
                response = response_t[0]
            elif response_t[1] is None:
                raise Exception('Error in adding to set')
            if response:
                self._print_if_verbose(f'Added to set with id {set_id} post with id {post_id}')
                response.close()
            else:
                self._print_if_verbose(f'Post with id = {post_id} was already in set with id {set_id}')
            yield (curr, len(posts_ids))
            curr += 1

    def get_action_posts(self, download_filter: Filter) -> Optional[List[dict]]:
        """
        Get list of posts for action based on filter.

        :param download_filter: Filter to use
        :return: List of posts or None if failed
        """
        filter_type = download_filter[0]
        filter_params = download_filter[1]
        if filter_type == 'pool':
            return self._get_pool_post_urls(filter_params['pool_id'])
        elif filter_type == 'tags':
            return self._get_tag_post_urls(filter_params['tags'])
        elif filter_type == 'popular':
            return self._get_popular_post_urls(filter_params['type'], filter_params['year'], filter_params['month'],
                                               filter_params['day'])
        else:
            raise NotImplementedError()
