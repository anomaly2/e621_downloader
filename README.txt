Downloader for e621 version 0.4
Configuration is taken from either conf.json or local_conf.json (preferred) file. Local_conf.json is used so you can fetch new version from repository without losing old config, and I won't accidentally send you my api key :).
Configuration is json file that contains following keys:
* useLogin - boolean, if true then next two fields are used to use login to download. Logins can have more allowed tags in search and can access some additional searches (for example user with login a can access votedup:a tag search)
*  login - string, user login
*  apiKey - string, api key to log in. Need to be generated here: https://e621.net/user/api_key
*  maxTries - integer, how many times should downloader try to access page before giving up. Definition of insanity is different when you have bad internet connection.
*  pageLimit - integer, how many pages should be downloaded at once. Upper limit and default value is 320.
*  verbose - boolean, if true then downloader print its action. If not it only prints when it screws up.
*  tagSavePattern - pattern which should be used to save download by tags results
*  poolSavePattern - pattern which should be used to save download by pool id results
*  useLogs - if true, app will save infos/warnings/errors in the log in log directory. Otherwise the will be printed on the default stderr.

Patterns can have following special fields:
{id} - inserts id of the post
{rating} - inserts 1 letter shortcut of post rating
{md5} - inserts md5 of the post
{index} - inserts number corresponding to order in which posts were downloaded. Useful for ordered tag search or pools.

Current usage:
Make sure you have python 3.6 or better installed.
Type "python sources\Downloader.py"

Code is under MIT license.