import sys
import os

sys.path.insert(0, os.path.join(os.path.dirname(sys.argv[0]), '..', 'sources'))

from SavePatternParser import *
import unittest


class TestSavePatternParser(unittest.TestCase):
    post1 = {
        'id': 1572502,
        'md5': '828e979e0c437cef7933d3645099ddd2',
        'rating': 'e'
    }

    def test_constant_pattern(self):
        parser = SavePatternParser('const')
        self.assertEqual('const',
                         parser.make_pattern(self.post1, 1), 'Const pattern')

    def test_md5(self):
        parser = SavePatternParser('{md5}')
        self.assertEqual('828e979e0c437cef7933d3645099ddd2',
                         parser.make_pattern(self.post1, 1), 'md5 pattern')

    def test_prefixed_md5(self):
        parser = SavePatternParser('PREFIX_{md5}')
        self.assertEqual('PREFIX_828e979e0c437cef7933d3645099ddd2',
                         parser.make_pattern(self.post1, 1), 'prefixed md5 pattern')

    def test_id_md5(self):
        parser = SavePatternParser('{id}_{md5}')
        self.assertEqual('1572502_828e979e0c437cef7933d3645099ddd2',
                         parser.make_pattern(self.post1, 1), 'id_md5 pattern')

    def test_index_rating_md5(self):
        parser = SavePatternParser('{index}_{rating}_{md5}')
        self.assertEqual('1_e_828e979e0c437cef7933d3645099ddd2',
                         parser.make_pattern(self.post1, 1), 'index_rating_md5 pattern')


if __name__ == '__main__':
    unittest.main()
