import sys
import os

sys.path.insert(0, os.path.join(os.path.dirname(sys.argv[0]), '..', 'sources'))

from DownloaderLogic import *
from Utils import *
import unittest
from shutil import copyfile, rmtree
from copy import deepcopy


class TestDownloaderLogic(unittest.TestCase):
    downloader: DownloaderLogic
    test_config: str
    download_path: str

    def setUp(self):
        try:
            config_paths = get_conf_paths()
            self.test_config = config_paths['test']
            self.download_path = os.path.join(os.path.dirname(sys.argv[0]), '..', 'tests', 'downloads')
            copyfile(config_paths['default'], self.test_config)
            os.makedirs(self.download_path)
            self.downloader = DownloaderLogic(True)
        except Exception:
            self.tearDown()
            raise

    def tearDown(self):
        os.remove(self.test_config)
        rmtree(self.download_path, True)

    def countPosts(self, dir_name: str) -> int:
        path = os.path.join(self.download_path, dir_name)
        return len([name for name in os.listdir(path) if os.path.isfile(os.path.join(path, name))])

    def file_exists(self, dir_name: str, post_name: str) -> bool:
        path = os.path.join(self.download_path, dir_name)
        return post_name in [name for name in os.listdir(path) if os.path.isfile(os.path.join(path, name))]

    def test_pool_download(self):
        for _ in self.downloader.download_pool(73, self.download_path):
            pass
        pool_name = 'The_Quiet'
        self.assertEqual(3, self.countPosts(pool_name))
        self.assertTrue(self.file_exists(pool_name, '1_506f3209848db6b4c69c4213237ba817.gif'))
        self.assertTrue(self.file_exists(pool_name, '2_31e2c92eafc336d198a6ba43ade83783.gif'))
        self.assertTrue(self.file_exists(pool_name, '3_36e92d874cb3f9604d0b5cbacf549434.gif'))

    def test_pool_download_with_pattern(self):
        new_config = deepcopy(self.downloader.config)
        new_config['poolSavePattern'] = '{rating}_{index}_{md5}'
        self.downloader.update_config(new_config)
        for _ in self.downloader.download_pool(73, self.download_path):
            pass
        pool_name = 'The_Quiet'
        self.assertEqual(3, self.countPosts(pool_name))
        self.assertTrue(self.file_exists(pool_name, 's_1_506f3209848db6b4c69c4213237ba817.gif'))
        self.assertTrue(self.file_exists(pool_name, 's_2_31e2c92eafc336d198a6ba43ade83783.gif'))
        self.assertTrue(self.file_exists(pool_name, 's_3_36e92d874cb3f9604d0b5cbacf549434.gif'))

    def test_tag_download(self):
        for _ in self.downloader.download_tags('bikuu', os.path.join(self.download_path, 'tag_test')):
            pass
        self.assertEqual(7, self.countPosts('tag_test'))
        self.assertTrue(self.file_exists('tag_test', '1419a0d832e82e6ada1a1947aaab2cae.png'))


if __name__ == '__main__':
    unittest.main()
